Source: libfile-modified-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: perl
Standards-Version: 3.9.6
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libfile-modified-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libfile-modified-perl.git
Homepage: https://metacpan.org/release/File-Modified

Package: libfile-modified-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends}
Multi-Arch: foreign
Description: module to check if files have changed
 File::Modified module is intended as a simple method for programs to detect
 whether configuration files (or modules they rely on) have changed. There are
 currently two methods of change detection implemented, mtime and MD5.
 The MD5 method will fall back to use timestamps if the Digest::MD5 module
 cannot be loaded.
 .
 There is another module, File::Signature, which has many similar features,
 so if this module doesn't do what you need, maybe File::Signature does. There
 also is quite some overlap between the two modules, code wise.
