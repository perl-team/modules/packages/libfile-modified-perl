libfile-modified-perl (0.10-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libfile-modified-perl: Add Multi-Arch: foreign.
  * Set upstream metadata fields: Repository-Browse.
  * Set upstream metadata fields: Repository.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 13 Oct 2022 23:52:24 +0100

libfile-modified-perl (0.10-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Alex Muntada ]
  * Remove inactive pkg-perl members from Uploaders.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Laurent Baillet ]
  * fix lintian file-contains-trailing-whitespace warning

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 13 Jun 2022 22:21:01 +0100

libfile-modified-perl (0.10-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Mon, 13 Jun 2022 17:53:09 +0200

libfile-modified-perl (0.10-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Lucas Kanashiro ]
  * Add debian/upstream/metadata
  * Import upstream version 0.10
  * Bump debhelper compatibility level to 9
  * Declare compliance with Debian policy 3.9.6
  * Mark as autopkg-testable

  [ gregor herrmann ]
  * debian/copyright: update Upstream-Contact.

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Sat, 05 Dec 2015 03:23:06 -0200

libfile-modified-perl (0.09-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Wed, 14 May 2014 21:05:55 +0200

libfile-modified-perl (0.08-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Strip trailing slash from metacpan URLs.
  * Drop no-defined-array.patch, applied upstream.
  * Update years of packaging copyright.
  * Update short description.

 -- gregor herrmann <gregoa@debian.org>  Mon, 12 May 2014 19:51:04 +0200

libfile-modified-perl (0.07-3) unstable; urgency=low

  * Team upload

  [ gregor herrmann ]
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).
  * debian/control: Added: ${misc:Depends} to Depends: field.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Damyan Ivanov ]
  * set source format to '3.0 (quilt)'
  * rules: convert from debhelper to dh
  * add patch fixing warnings when defined(@array) is used with perl 5.18
  * drop trailing slash from metacpan URL
  * copyright: convert to machine-parseable format
  * declare conformance with Policy 3.9.5

 -- Damyan Ivanov <dmn@debian.org>  Wed, 29 Jan 2014 11:56:14 +0200

libfile-modified-perl (0.07-2) unstable; urgency=low

  * Bugfix release
  * debian/watch: Updated to new schema (closes: #449850)
  * debian/rules: FTBS fix (closes: #468034)
  * debian/control:
   + Standards-Version increased to 3.7.3 (no changes)
   + Added additional headers
  * debian/compat: increased to 5 (changed debhelper dependency)

 -- Krzysztof Krzyżaniak (eloy) <eloy@debian.org>  Fri, 21 Mar 2008 11:52:27 +0100

libfile-modified-perl (0.07-1) unstable; urgency=low

  * Initial Release (closes: #337710).

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Sat,  5 Nov 2005 20:20:59 +0100
